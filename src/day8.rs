use anyhow::Result;
use helpfull_elf;

fn main() -> Result<()> {
    let lines = helpfull_elf::read_lines_stdin();

    let mut forrest: Vec<Vec<_>> = Vec::with_capacity(lines.len());

    for line in &lines {
        let mut trees_split = line.split("").collect::<Vec<_>>();

        trees_split.remove(0);
        trees_split.pop();

        let parsed_trees = trees_split
            .iter()
            .map(|tree| {
                tree.parse::<usize>()
                    .expect("What a tree that we could not parse")
            })
            .collect::<Vec<_>>();

        forrest.push(parsed_trees)
    }

    let mut visible = lines.len() * 2 + (lines[0].len() - 2) * 2;

    let check_until_height = forrest.len() - 1;
    let check_until_with = forrest[1].len() - 1;

    for height in 1..check_until_height {
        for with in 1..check_until_with {
            let tree = forrest[height][with];

            let mut found_bigger = false;
            for i in with + 1..check_until_with + 1 {
                if tree <= forrest[height][i] {
                    found_bigger = true;
                }
            }

            if !found_bigger {
                visible += 1;
                continue;
            }

            if tree > forrest[height][with - 1] {
                let mut found_bigger = false;
                for i in 0..with - 1 {
                    if tree <= forrest[height][i] {
                        found_bigger = true;
                    }
                }

                if !found_bigger {
                    visible += 1;
                    continue;
                }
            }

            if tree > forrest[height - 1][with] {
                let mut found_bigger = false;
                for i in 0..height - 1 {
                    if tree <= forrest[i][with] {
                        found_bigger = true;
                    }
                }

                if !found_bigger {
                    visible += 1;
                    continue;
                }
            }

            if tree > forrest[height + 1][with] {
                let mut found_bigger = false;
                for i in height + 1..check_until_height + 1 {
                    if tree <= forrest[i][with] {
                        found_bigger = true;
                    }
                }

                if !found_bigger {
                    visible += 1;
                    continue;
                }
            }
        }
    }

    let mut best_scrore = 0;

    let mut found_tree = (0, 0);

    for height in 1..forrest.len() - 1 {
        for with in 1..forrest[1].len() - 1 {
            let tree = forrest[height][with];

            let mut scores = vec![
                if with == 0 { 0 } else { 1 },
                if with == check_until_with { 0 } else { 1 },
                if height == 0 { 0 } else { 1 },
                if height == check_until_height { 0 } else { 1 },
            ];

            let let_range = (0..with).rev();
            for (index, value) in let_range.enumerate() {
                scores[0] = index + 1;
                if tree <= forrest[height][value] {
                    break;
                }
            }

            let range_right = with + 1..check_until_with + 1;
            for (index, value) in range_right.enumerate() {
                scores[1] = index + 1;
                if tree <= forrest[height][value] {
                    break;
                }
            }

            let range_top = (0..height).rev();
            for (index, value) in range_top.enumerate() {
                scores[2] = index + 1;
                if tree <= forrest[value][with] {
                    break;
                }
            }

            let range_bottom = height + 1..check_until_height + 1;
            for (index, value) in range_bottom.enumerate() {
                scores[3] = index + 1;
                if tree <= forrest[value][with] {
                    break;
                }
            }
            let mut tmp = 1;

            for score in 0..4 {
                if scores[score] == 0 {
                    continue;
                }

                tmp = tmp * scores[score]
            }

            if tmp > best_scrore {
                found_tree.0 = height;
                found_tree.1 = with;
                best_scrore = tmp
            }
        }
    }

    for tree_line in forrest.iter().enumerate() {
        for tree in tree_line.1.iter().enumerate() {
            if found_tree.0 == tree_line.0 && found_tree.1 == tree.0 {
                print!("\x1b[93mx\x1b[0m")
            } else {
                print!("{}", tree.1)
            }
        }
        println!("");
    }

    println!("Result part 1: {visible}");

    println!("Result part 2: {best_scrore}");

    Ok(())
}
