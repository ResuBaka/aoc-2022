use std::env::args;
use std::fs::read_to_string;
use std::io;
use std::ops::Add;

fn main() -> Result<(), io::Error> {
    let file = read_to_string("input/day1.txt")?;
    let mut cal: Vec<u64> = vec![];
    let mut elf = 0;

    for line in file.lines() {
        match line {
            "" => {
                elf += 1;
            }
            _ => match cal.get(elf) {
                Some(asd) => {
                    cal[elf] = asd.add(line.parse::<u64>().expect("to parse line"));
                }
                _ => {
                    cal.push(line.parse::<u64>().expect("To parse line"));
                }
            },
        }
    }

    cal.sort_by(|a, b| a.partial_cmp(b).unwrap());
    println!("Elf with the most cal: {}", cal[cal.len() - 1]);
    println!(
        "Elf the cals of the top 3 Elf's: {}",
        cal[cal.len() - 1] + cal[cal.len() - 2] + cal[cal.len() - 3]
    );
    Ok(())
}
