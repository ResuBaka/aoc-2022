use std::io::stdin;

pub fn read_lines_stdin() -> Vec<String> {
    stdin()
        .lines()
        .filter(|line| line.is_ok())
        .map(|line| line.unwrap())
        .collect::<Vec<_>>()
}
