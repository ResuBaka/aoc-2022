use std::collections::HashSet;

fn main() {
    let input = include_str!("../input/4.txt");

    let mut part1 = 0;
    let mut part2 = 0;

    let _ = input
        .lines()
        .map(|line| {
            let split = line.split(",").collect::<Vec<&str>>();

            let asd = split
                .iter()
                .map(|range| {
                    range
                        .split("-")
                        .map(|str| str.parse::<u64>().unwrap())
                        .collect::<Vec<u64>>()
                })
                .map(|range| {
                    if range[0] == range[1] {
                        range
                    } else {
                        let mut new_range = vec![range[0].clone()];
                        let mut next_value = range[0];
                        loop {
                            next_value += 1;
                            if next_value >= range[1] {
                                break;
                            }
                            new_range.push(next_value)
                        }
                        new_range.push(range[1]);

                        new_range
                    }
                })
                .map(|asd| {
                    let mut hashset = HashSet::new();

                    for a in asd {
                        hashset.insert(a);
                    }

                    hashset
                })
                .collect::<Vec<_>>();

            for item in asd[0].clone() {
                if asd[1].contains(&item) {
                    part2 += 1;
                    break;
                }
            }

            if asd[0].is_superset(&asd[1]) || asd[0].is_subset(&asd[1]) {
                part1 += 1;
            }
        })
        .collect::<()>();

    println!("Result part one {}", part1);
    println!("Result part two {}", part2);
}
