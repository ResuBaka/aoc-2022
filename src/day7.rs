use helpfull_elf;
use std::collections::HashMap;
use std::iter::once;

#[derive(Debug)]
enum Node {
    Dir(Vec<String>, HashMap<String, Node>),
    File(String, usize),
}

impl Node {
    fn size(&self) -> usize {
        match self {
            Node::Dir(_, map) => map.values().map(|node| node.size()).sum(),
            Node::File(_, size) => *size,
        }
    }

    fn total_at_most(&self, max: usize) -> usize {
        match self {
            Node::Dir(_, map) => {
                let size = self.size();
                (if size <= max { size } else { 0 })
                    + map
                        .values()
                        .map(|item| item.total_at_most(max))
                        .sum::<usize>()
            }
            Node::File(_, _) => 0,
        }
    }

    fn smallest_directory_of_at_least(&self, space_to_delete: usize) -> Option<usize> {
        if self.size() < space_to_delete {
            None
        } else {
            match self {
                Node::Dir(_, map) => map
                    .values()
                    .filter_map(|f| f.smallest_directory_of_at_least(space_to_delete))
                    .chain(once(self.size()))
                    .min(),
                Node::File(_, _) => None,
            }
        }
    }
}

fn main() {
    let lines = helpfull_elf::read_lines_stdin();

    let mut root = Node::Dir(vec!["/".into()], Default::default());

    let mut currentDir: &mut Node = &mut root;
    for line in lines {
        if line.starts_with("$ cd") {
            let dir = line.split(' ').nth(2).unwrap();
            if dir == "/" {
                currentDir = &mut root
            } else if dir == ".." {
                let Node::Dir(cur, members) = currentDir else { panic!() };
                let parent_path: Vec<String> = cur.iter().rev().skip(1).rev().cloned().collect();
                for loc in parent_path {
                    if loc == "/" {
                        drop(currentDir);
                        currentDir = &mut root;
                    } else {
                        let Node::Dir(_cur, members) = currentDir else { panic!() };
                        currentDir = members.get_mut(&loc).unwrap();
                    }
                }
            } else {
                let Node::Dir(cur, members) = currentDir else { panic!() };
                let new_dir = cur
                    .clone()
                    .into_iter()
                    .chain(once(dir.to_owned()))
                    .collect();
                currentDir = members
                    .entry(dir.to_owned())
                    .or_insert(Node::Dir(new_dir, Default::default()))
            }
        } else if line.starts_with("$ ls") {
            continue;
        } else {
            let (sz, name) = line.split_once(' ').unwrap();
            if sz == "dir" {
                continue;
            }
            let sz: usize = sz.parse().unwrap();
            let name = name.trim().to_owned();
            let Node::Dir(_cur, members) = currentDir else { panic!() };
            let filenode = Node::File(name.clone(), sz);
            members.insert(name, filenode);
        }
    }

    println!("Result part 1: {:?}", root.total_at_most(100000));

    let total_space = 70000000;
    let required_space = 30000000;
    let current_space = root.size();
    let space_to_delete = current_space - (total_space - required_space);

    println!(
        "Result part 2: {:?}",
        root.smallest_directory_of_at_least(space_to_delete)
    );
}
