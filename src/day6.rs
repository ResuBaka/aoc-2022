use helpfull_elf;
struct MoveCommand {
    amount: usize,
    from: usize,
    to: usize,
}

fn get_matching_chars_in_order(string: &String, size: usize) -> usize {
    let mut result = 0;
    let mut buffer = Vec::with_capacity(size);
    for (index, char) in string.chars().enumerate() {
        if buffer.contains(&char) {
            let pos = buffer.iter().position(|item| item == &char).unwrap() + 1;
            buffer = buffer[pos..].into()
        }

        buffer.push(char);

        if buffer.len() == size {
            result = index + 1;
            break;
        }
    }

    result
}

fn main() {
    let lines = helpfull_elf::read_lines_stdin();

    println!(
        "Result part 1: {}",
        get_matching_chars_in_order(&lines[0], 4)
    );
    println!(
        "Result part 2: {}",
        get_matching_chars_in_order(&lines[0], 14)
    )
}
