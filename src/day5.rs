use std::io::stdin;

struct MoveCommand {
    amount: usize,
    from: usize,
    to: usize,
}

fn main() {
    let input = stdin();

    let mut cargo_lines = vec![];
    let mut move_lines = vec![];
    let lines = input
        .lines()
        .filter(|line| line.is_ok())
        .map(|line| line.unwrap())
        .collect::<Vec<_>>();

    for (index, line) in lines.iter().enumerate() {
        if line.len() == 0 {
            cargo_lines = lines[0..index].to_vec();
            move_lines = lines[index + 1..].to_vec();

            break;
        }
    }

    cargo_lines.reverse();

    let found = cargo_lines[0]
        .split(" ")
        .filter(|asd| !asd.is_empty())
        .collect::<Vec<_>>()
        .len();
    let mut structure = vec![vec![]; found];

    // println!("found : {found}");

    for line_index in 0..found {
        // println!("");
        // println!("line_index: {line_index}");

        if let None = cargo_lines.get(line_index + 1) {
            break;
        }

        let line_to_work_with = cargo_lines[line_index + 1].clone();

        // println!("{line_to_work_with}");

        for line_pos in 0..found {
            let start = if line_pos == 0 {
                0
            } else {
                1 * line_pos + line_pos * 3
            } + 1;

            // println!("Start {}  end {}, line length: {}", start, start+1, line_to_work_with.len());
            if line_to_work_with.len() <= start + 1 {
                break;
            }

            let string_to_add = line_to_work_with[start..start + 1].to_string();

            // println!("Chars to get {string_to_add}");

            if string_to_add != " " {
                structure[line_pos].push(string_to_add);
            }
        }
    }

    let mut structure_part2 = structure.clone();

    for move_line in &move_lines {
        let move_command = get_move(&move_line);

        for _ in 0..move_command.amount {
            let value = structure[move_command.from].pop().unwrap();
            structure[move_command.to].push(value)
        }
    }

    print!("Result part one: ");
    for s in structure {
        print!("{}", s[s.len() - 1])
    }

    println!("");

    for move_line in &move_lines {
        let move_command = get_move(&move_line);

        let mut tmp_cargo = vec![];
        for _ in 0..move_command.amount {
            tmp_cargo.push(structure_part2[move_command.from].pop().unwrap());
        }

        tmp_cargo.reverse();

        for cargo in tmp_cargo {
            structure_part2[move_command.to].push(cargo)
        }
    }

    print!("Result part two: ");
    for s in structure_part2 {
        print!("{}", s[s.len() - 1])
    }

    println!("")
}

fn get_move(line: &String) -> MoveCommand {
    let split = line.split(" ").collect::<Vec<&str>>();

    MoveCommand {
        amount: split[1].parse().unwrap(),
        from: split[3].parse::<usize>().unwrap() - 1 as usize,
        to: split[5].parse::<usize>().unwrap() - 1 as usize,
    }
}
