use std::env::args;
use std::fs::read_to_string;
use std::io;
use std::ops::Add;

#[derive(Debug, Copy, Clone)]
enum Move {
    rock,
    paper,
    scissors,
    whatamove,
}

#[derive(Debug, Copy, Clone)]
enum Points {
    win,
    loss,
    draw,
    notsosure,
}

fn main() -> Result<(), io::Error> {
    let file = read_to_string("input/day2.txt")?;

    let win = 6;
    let draw = 3;
    let lose = 0;
    let rock_a_x = 1;
    let paper_b_y = 2;
    let scissors_c_z = 3;

    let mut points = 0;
    let mut points2 = 0;

    for line in file.lines() {
        let split_line = line.split("").collect::<Vec<&str>>();

        let points_to_add = match (split_line[1], split_line[3]) {
            ("A", "X") => draw + rock_a_x,
            ("A", "Y") => win + paper_b_y,
            ("A", "Z") => lose + scissors_c_z,
            ("B", "X") => lose + rock_a_x,
            ("B", "Y") => draw + paper_b_y,
            ("B", "Z") => win + scissors_c_z,
            ("C", "X") => win + rock_a_x,
            ("C", "Y") => lose + paper_b_y,
            ("C", "Z") => draw + scissors_c_z,
            _ => {
                println!("What happend here: {:?}", split_line);
                0
            }
        };

        points += points_to_add
    }

    println!("My points are: {}", points);

    for line in file.lines() {
        let split_line = line.split("").collect::<Vec<&str>>();

        let mymove = match split_line[3] {
            "X" => Points::loss,
            "Y" => Points::draw,
            "Z" => Points::win,
            _ => Points::notsosure,
        };
        let enemy_type = match split_line[1] {
            "A" => Move::rock,
            "B" => Move::paper,
            "C" => Move::scissors,
            _ => Move::whatamove,
        };

        let points_to_add = match (enemy_type, mymove) {
            (Move::paper, Points::loss) => lose + rock_a_x,
            (Move::paper, Points::draw) => draw + paper_b_y,
            (Move::paper, Points::win) => win + scissors_c_z,
            (Move::rock, Points::loss) => lose + scissors_c_z,
            (Move::rock, Points::draw) => draw + rock_a_x,
            (Move::rock, Points::win) => win + paper_b_y,
            (Move::scissors, Points::loss) => lose + paper_b_y,
            (Move::scissors, Points::draw) => draw + scissors_c_z,
            (Move::scissors, Points::win) => win + rock_a_x,
            _ => {
                println!("What happend here: {:?}, {:?}", split_line, mymove);
                0
            }
        };

        points2 += points_to_add
    }

    println!("My points are: {}", points2);

    Ok(())
}
