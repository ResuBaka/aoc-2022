use anyhow::Result;
use helpfull_elf;
use std::borrow::{Borrow, BorrowMut};
use std::collections::HashSet;
use std::str::FromStr;

#[derive(Debug, Copy, Clone)]
enum Moves {
    Up(usize),
    Down(usize),
    Left(usize),
    Right(usize),
}

#[derive(Default, Debug, Clone, Copy, PartialEq)]
struct Position {
    x: isize,
    y: isize,
}

impl Position {
    pub fn move_direction(&mut self, m: &Moves) {
        match m {
            Moves::Up(to_move) => self.x += *to_move as isize,
            Moves::Down(to_move) => self.x -= *to_move as isize,
            Moves::Left(to_move) => self.y -= *to_move as isize,
            Moves::Right(to_move) => self.y += *to_move as isize,
        }
    }

    pub fn move_direction_tail(self, tail: &mut Position) {
        let x = self.x - tail.x;
        let y = self.y - tail.y;

        if x == 2 || x == -2 {
            if y == 1 || y == -1 {
                tail.x += if x.is_positive() { 1 } else { -1 };
                tail.y += if y.is_positive() { 1 } else { -1 };
            } else {
                tail.x += if x.is_positive() { 1 } else { -1 };
            }
        }

        if y == 2 || y == -2 {
            if x == 1 || x == -1 {
                tail.x += if x.is_positive() { 1 } else { -1 };
                tail.y += if y.is_positive() { 1 } else { -1 };
            } else {
                tail.y += if y.is_positive() { 1 } else { -1 };
            }
        }
    }
}

impl FromStr for Moves {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let (m, number) = s.split_once(" ").unwrap();

        match m {
            "U" => Ok(Moves::Up(number.parse()?)),
            "D" => Ok(Moves::Down(number.parse()?)),
            "L" => Ok(Moves::Left(number.parse()?)),
            "R" => Ok(Moves::Right(number.parse()?)),
            _ => panic!("Good unexpected move {}", m),
        }
    }
}

impl Moves {
    fn repeats(&self) -> usize {
        match self {
            Moves::Up(size) => *size,
            Moves::Down(size) => *size,
            Moves::Left(size) => *size,
            Moves::Right(size) => *size,
        }
    }

    fn name(&self) -> &str {
        match self {
            Moves::Up(_) => "Up",
            Moves::Down(_) => "Down",
            Moves::Left(_) => "Left",
            Moves::Right(_) => "Right",
        }
    }
}

impl ToString for Position {
    fn to_string(&self) -> String {
        format!("{}x{}", self.x, self.y)
    }
}

fn main() -> Result<()> {
    let lines = helpfull_elf::read_lines_stdin();

    let moves = lines
        .iter()
        .map(|item| {
            item.parse::<Moves>()
                .expect(format!("Should be able to parse move {}", item).as_str())
        })
        .flat_map(|m| {
            let mut tmp = vec![];

            let a: Moves = m;

            for _ in 0..a.repeats() {
                match m {
                    Moves::Up(_) => tmp.push(Moves::Up(1)),
                    Moves::Down(_) => tmp.push(Moves::Down(1)),
                    Moves::Left(_) => tmp.push(Moves::Left(1)),
                    Moves::Right(_) => tmp.push(Moves::Right(1)),
                }
            }

            tmp
        })
        .collect::<Vec<Moves>>();

    let result = lines
        .iter()
        .map(|item| item.split_once(" ").unwrap().1.parse::<usize>().unwrap())
        .sum::<usize>();

    println!("{} {result}", moves.len());

    let mut visited = HashSet::new();
    visited.insert("0x0".into());
    let mut head_p = Position::default();
    let mut tail_p = Position::default();
    for m in &moves {
        head_p.move_direction(m);
        head_p.move_direction_tail(&mut tail_p);
        visited.insert(tail_p.to_string());
    }

    println!("Result part 1 {}", visited.len());
    let mut visited = HashSet::new();
    visited.insert("0x0".into());
    let mut my_rope = Vec::from_iter((0..10).map(|_| Position::default()));
    for m in &moves {
        my_rope[0].move_direction(&m);
        for index in 0..(my_rope.len() - 1) {
            my_rope[index].move_direction_tail(my_rope[index + 1].borrow_mut());
            visited.insert(my_rope[my_rope.len() - 1].to_string());
        }
    }

    println!("Result part 2: {}", visited.len());

    Ok(())
}
