use std::collections::{HashMap, HashSet};

const LOWERCASE: u8 = b'a' - 1;
const UPPERCASE: u8 = b'A' - 1;

fn main() {
    let mut points_map = HashMap::new();

    points_map.insert('a', 1);
    points_map.insert('b', 2);
    points_map.insert('c', 3);
    points_map.insert('d', 4);
    points_map.insert('e', 5);
    points_map.insert('f', 6);
    points_map.insert('g', 7);
    points_map.insert('h', 8);
    points_map.insert('i', 9);
    points_map.insert('j', 10);
    points_map.insert('k', 11);
    points_map.insert('l', 12);
    points_map.insert('m', 13);
    points_map.insert('n', 14);
    points_map.insert('o', 15);
    points_map.insert('p', 16);
    points_map.insert('q', 17);
    points_map.insert('r', 18);
    points_map.insert('s', 19);
    points_map.insert('t', 20);
    points_map.insert('u', 21);
    points_map.insert('v', 22);
    points_map.insert('w', 23);
    points_map.insert('x', 24);
    points_map.insert('y', 25);
    points_map.insert('z', 26);
    points_map.insert('A', 27);
    points_map.insert('B', 28);
    points_map.insert('C', 29);
    points_map.insert('D', 30);
    points_map.insert('E', 31);
    points_map.insert('F', 32);
    points_map.insert('G', 33);
    points_map.insert('H', 34);
    points_map.insert('I', 35);
    points_map.insert('J', 36);
    points_map.insert('K', 37);
    points_map.insert('L', 38);
    points_map.insert('M', 39);
    points_map.insert('N', 40);
    points_map.insert('O', 41);
    points_map.insert('P', 42);
    points_map.insert('Q', 43);
    points_map.insert('R', 44);
    points_map.insert('S', 45);
    points_map.insert('T', 46);
    points_map.insert('U', 47);
    points_map.insert('V', 48);
    points_map.insert('W', 49);
    points_map.insert('X', 50);
    points_map.insert('Y', 51);
    points_map.insert('Z', 52);

    let mut input = include_str!("../input/3.txt");

    let mut part_one_result = 0;

    for line in input.lines() {
        let split_lines = line.split_at(line.len() / 2);

        'outer: for (index, char) in split_lines.0.chars().enumerate() {
            for (index2, char2) in split_lines.1.chars().enumerate() {
                if char2 == char {
                    if let Some(points_to_add) = points_map.get(&char) {
                        part_one_result += points_to_add;
                        break 'outer;
                    }
                }
            }
        }
    }

    let chained_functions = input
        .lines()
        .map(|line| {
            let (a, b) = line.split_at(line.len() / 2);
            let a = a.chars().collect::<HashSet<char>>();
            b.chars().find(|char| a.contains(char)).unwrap()
        })
        .map(|char| {
            let value = if char.is_lowercase() {
                char as u8 - LOWERCASE
            } else {
                char as u8 - UPPERCASE + 26
            };

            value as u32
        })
        .sum::<u32>();

    println!("My points {}", part_one_result);
    println!("My points {}", chained_functions);
    println!("");
    let mut part_two_result = 0;

    let mut group = vec![];

    for line in input.lines() {
        group.push(line);

        if group.len() == 3 {
            'outer: for char in group.get(0).unwrap().chars() {
                for char2 in group.get(1).unwrap().chars() {
                    for char3 in group.get(2).unwrap().chars() {
                        if char2 == char && char3 == char {
                            if let Some(points_to_add) = points_map.get(&char) {
                                part_two_result += points_to_add;
                                break 'outer;
                            }
                        }
                    }
                }
            }
            group.clear();
        }
    }

    let chained_functions = input
        .lines()
        .collect::<Vec<_>>()
        .chunks(3)
        .map(|line| {
            let a = line[0].chars().collect::<HashSet<char>>();
            let c = line[1].chars().collect::<HashSet<char>>();
            line[2]
                .chars()
                .find(|char| a.contains(char) && c.contains(char))
                .unwrap()
        })
        .map(|char| {
            let value = if char.is_lowercase() {
                char as u8 - LOWERCASE
            } else {
                char as u8 - UPPERCASE + 26
            };

            value as u32
        })
        .sum::<u32>();

    println!("My points {}", part_two_result);
    println!("My points {}", chained_functions);
}
