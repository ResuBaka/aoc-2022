use anyhow::Result;
use helpfull_elf;
use std::io;
use std::io::Write;
use std::thread::sleep;
use std::time::Duration;

#[derive(Debug, Clone)]
enum Instruction {
    AddX(isize, u8),
    Noop,
}

fn main() -> Result<()> {
    let instructions = helpfull_elf::read_lines_stdin()
        .iter()
        .map(|line| {
            let mut split = line.split(" ");

            match split.nth(0).unwrap() {
                "noop" => Instruction::Noop,
                "addx" => Instruction::AddX(
                    split
                        .nth(0)
                        .unwrap()
                        .parse::<isize>()
                        .expect("This needs to parse"),
                    2,
                ),
                _ => unreachable!(),
            }
        })
        .collect::<Vec<Instruction>>();

    let mut instructions_iter = instructions.into_iter();

    let mut has_instruction_too_call = instructions_iter.next();

    let mut cycle: usize = 1;
    let mut register_x: isize = 1;

    let mut result_p1 = 0;

    let mut next_cycle_to_add = 20;
    let mut row = 0;
    let mut next_row_at = 40;

    let mut crt = Vec::from_iter((0..6).map(|_| Vec::from_iter((0..40).map(|_| '.'))));

    // println!("{} \n{:?}", has_instruction_too_call.is_some(), instructions);
    while let Some(instruction) = has_instruction_too_call {
        if next_cycle_to_add == cycle {
            result_p1 += cycle as isize * register_x;
            next_cycle_to_add += 40;
        }

        if row < crt.len() {
            let left = register_x - 1;
            let middle = register_x;
            let right = register_x + 1;

            let insert_at = if row == 0 {
                cycle - 1
            } else {
                cycle - (next_row_at - 40) - 1
            };

            for insert in vec![left, middle, right] {
                if insert < 0 {
                    continue;
                }

                if let Some(_) = crt[row].get(insert as usize) {
                    if insert == insert_at as isize {
                        crt[row][insert as usize] = '#'
                    }
                }
            }
        }

        // print_crt(&crt, true);

        if cycle == next_row_at {
            row += 1;
            next_row_at += 40;
        }

        match instruction {
            Instruction::AddX(x, y) => {
                if y == 2 {
                    has_instruction_too_call = Some(Instruction::AddX(x, 1));
                } else {
                    register_x += x;
                    has_instruction_too_call = instructions_iter.next()
                }
            }
            Instruction::Noop => has_instruction_too_call = instructions_iter.next(),
        }
        cycle += 1;
        // sleep(Duration::from_millis(50))
    }

    println!("Result part 1: {result_p1}");

    println!("Result part 2:");

    print_crt(&crt, false);

    Ok(())
}

fn print_crt(crt: &Vec<Vec<char>>, reset: bool) {
    if reset {
        print!("{esc}[2J{esc}[1;1H", esc = 27 as char);
    }
    for row in crt {
        for line_item in row {
            print!("{line_item}")
        }
        print!("\n")
    }
    if reset {
        let _ = io::stdout().flush();
    }
}
